extends Node2D

@export var noise_height_texture : NoiseTexture2D
var noise : Noise

var width : int = 400
var height : int = 400

var noise_value_array : Array = []

@onready var tile_map : TileMap = $TileMap
var source_id : int = 0
var land_atlas : Vector2i = Vector2i(0, 0)
var water_atlas : Vector2i = Vector2i(4, 0)

var rng : RandomNumberGenerator = RandomNumberGenerator.new()

var enemy = preload("res://Scenes/enemy.tscn")

#func spawn_player_on_ground():
	#$Player.position = Vector2i(25, 25)


func generate_world():
	for x in range(-width/2, width/2):
		for y in range(-height/2, height/2):
			var noise_value : float = noise.get_noise_2d(x, y)
			if noise_value >= -0.3:
				tile_map.set_cell(0, Vector2i(x, y), source_id, land_atlas)
			elif noise_value < -0.3:
				tile_map.set_cell(0, Vector2i(x, y), source_id, land_atlas)
				# spawn water
				#tile_map.set_cell(0, Vector2i(x, y), source_id, water_atlas)
 
func _ready():
	noise = noise_height_texture.noise
	noise.seed = rng.randi_range(0, 1000)
	generate_world()
	#spawn_player_on_ground()



#func _process(delta):
	#pass


func _on_enemy_spawn_timer_timeout():
	var new_enemy = enemy.instantiate()
	new_enemy.position = $Player.position + Vector2(500, 0).rotated((randi_range(0, 2*PI)))
	add_child(new_enemy)
