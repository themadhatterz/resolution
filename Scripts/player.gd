extends CharacterBody2D

#@export var game_manager: GameManager
const SPEED = 200.0
var experience = 0
var experience_max = 100
var level = 1


var bullet = preload("res://Scenes/bullet.tscn")

#signal experience_changed
#signal level_changed

func _ready():
	pass


func _physics_process(delta):

	var input_direction = Vector2(0.0, 0.0)
	if Input.is_action_pressed("move_right"):
		input_direction.x += 1
	if Input.is_action_pressed("move_left"):
		input_direction.x -= 1
	if Input.is_action_pressed("move_down"):
		input_direction.y += 1
	if Input.is_action_pressed("move_up"):
		input_direction.y -= 1


	velocity = input_direction.normalized() * SPEED

	move_and_slide()


#func experience_gained():
	#experience += 5
	#if experience >= experience_max:
		#experience = 0
		#experience_max = experience_max * 2
		#level += 1
		#level_changed.emit()
		#var shuriken = get_node("ShurikenShield")
		#shuriken.speed = shuriken.speed * level
	#experience_changed.emit()


func _on_shoot_bullet_timeout():
	var dirs = [Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT]
	for dir in dirs:
		var new_bullet = bullet.instantiate()
		new_bullet.shoot(dir)
		add_child(new_bullet)
